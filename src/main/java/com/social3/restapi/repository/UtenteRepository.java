package com.social3.restapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.social3.restapi.model.Utente;

public interface UtenteRepository extends JpaRepository<Utente, Long> {
	
	Utente findByUsernameAndPassword(String username, String password);
	

}
