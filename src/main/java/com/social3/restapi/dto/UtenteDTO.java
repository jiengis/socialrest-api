package com.social3.restapi.dto;

import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

import com.social3.restapi.model.Utente;

import lombok.Data;

@Data
public class UtenteDTO {
	
	private Long id;
	private Date  dataInserimento;
	private Date dataAggiornamento;
	private String username;
	private String password;
	private String firstname;
	private String lastname;
	private Date dob;
	private Collection<Long> pagineCreate;
	private Collection<Long> commenti;
	
	public static UtenteDTO convert(Utente utente) {
		
		UtenteDTO utenteDTO = new UtenteDTO();

		utenteDTO.setId(utente.getId());
		utenteDTO.setCommenti(utente.getCommenti().stream().map(c -> c.getId()).collect(Collectors.toList()));
		utenteDTO.setPagineCreate(utente.getPagineCreate().stream().map(c -> c.getId()).collect(Collectors.toList()));
		
		return utenteDTO;
	}

}
