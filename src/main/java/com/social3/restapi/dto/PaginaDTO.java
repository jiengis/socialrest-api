package com.social3.restapi.dto;

import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

import com.social3.restapi.model.Pagina;
import com.social3.restapi.model.Utente;

import lombok.Data;

@Data
public class PaginaDTO {

	
	private Long id;
	private Date dataInserimento;
	private Date dataAggiornamento;
	private String titolo;
	private String testo;
	
	private Collection<Long> commenti;
	
	private Utente autore;
	
	public static PaginaDTO convert(Pagina pagina) {
		
		PaginaDTO paginaDTO = new PaginaDTO();
		paginaDTO.setId(pagina.getId());
		paginaDTO.setCommenti(pagina.getCommenti().stream().map(c -> c.getId()).collect(Collectors.toList()));
		paginaDTO.setAutore(pagina.getAutore());
		
		return paginaDTO;
		
	}
	
}
