package com.social3.restapi.model;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
public class Utente extends AbstractModelObject {

	@Column(unique = true, length = 30)
	private String username;
	private String password;

	private String firstname;
	private String lastname;

	private Date dob;

	@OneToMany(mappedBy = "autore")
	private Collection<Pagina> pagineCreate;

	@OneToMany(mappedBy = "commentatore")
	private Collection<Commento> commenti;
}
