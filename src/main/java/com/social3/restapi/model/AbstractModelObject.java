package com.social3.restapi.model;

import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import lombok.Data;

@MappedSuperclass
@Data
public abstract class AbstractModelObject {
	
	@Id
	@GeneratedValue
	private Long id;
	
	private Date dataInserimento;
	private Date dataAggiornamento;

	
}
