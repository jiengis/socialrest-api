package com.social3.restapi.model;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
public class Pagina extends AbstractModelObject {

	private String titolo;
	private String testo;

	@OneToMany(mappedBy = "paginaCommentata")
	private Collection<Commento> commenti;
	
	@ManyToOne
	private Utente autore;

}
