package com.social3.restapi.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
public class Commento extends AbstractModelObject {

	private String testo;
	
	@ManyToOne
	private Utente commentatore;
	@ManyToOne
	private Pagina paginaCommentata;
}
