package com.social3.restapi.controller;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.social3.restapi.dto.UtenteDTO;
import com.social3.restapi.model.Utente;
import com.social3.restapi.repository.UtenteRepository;

import lombok.extern.log4j.Log4j2;

@RestController
@Log4j2
public class UtenteController {
	
	@Autowired
	private UtenteRepository utenteRepository;
	
	@GetMapping("utenti")
	public Collection<UtenteDTO> retrieveAll(){
		log.debug("/utenti retrieve");
		return utenteRepository.findAll().stream().map(UtenteDTO::convert).collect(Collectors.toList());
	}
	
//	@GetMapping("utenti/{id}")
//	public Optional<Utente> retrieve(@PathVariable Long id) {
//		log.debug("/utenti/{id} retrieve");
//		return utenteRepository.findById(id);
//  }
	
	@GetMapping("utenti/{id}")
	public ResponseEntity<?> retrieve(@PathVariable Long id) {
		log.debug("/utenti/{id} retrieve");
		Optional<Utente> optionalUtente = utenteRepository.findById(id);
		if (optionalUtente.isPresent()) {
			return ResponseEntity.ok(UtenteDTO.convert(optionalUtente.get()));
		} else {
			return ResponseEntity.notFound().build();
		}
		
	}

}
