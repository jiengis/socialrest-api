package com.social3.restapi.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.social3.restapi.repository.UtenteRepository;

@RestController
public class StatusController {

	@Autowired
	private UtenteRepository utenteRepository;

	@GetMapping("status")
	public Map<String, Object> status() {
		Map<String, Object> map = new HashMap<>();
		map.put("utente-count", utenteRepository.count());
		return map;
	}
}
